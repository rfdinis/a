#include <opencv2/opencv.hpp>
#include <opencv2/tracking.hpp>
#include <opencv2/core/ocl.hpp>

#include <cstring>
#include <string>
#include <vector>

#include <thread>
#include <mutex>
#include <exception>

#include "CamTracking.hpp"

//static std::mutex g_imshowMut;



int main(int argc, char *argv[])
{
    std::string trackerTypes[8] = {"BOOSTING", "MIL", "KCF", "TLD","MEDIANFLOW", "GOTURN", "MOSSE", "CSRT"};
    int t = 0;
    if(argc <= 1)
    {
        std::cout << "choose a tracker:" << std::endl;
        for(int i=0; i<8; i++)
        {
            std::cout << "id=" << i << ": " << trackerTypes[i] << std::endl;
        }
        std::cout << "your choice: ";
        std::cin >> t;
    }
    else if(argc == 2)
    {
        t = (int)strtol( argv[1] , NULL, 0 ) ;
    }
    
    // Create a tracker
    std::string trackerType = trackerTypes[t];
    std::cout << "Executing " << trackerType << std::endl;
    cv::VideoCapture video("../vid3.mp4");
    cv::VideoCapture video2("../vid2.mp4");

    if(!video.isOpened() || !video2.isOpened())// Exit if video is not opened
    { 
        std::cout << "Could not read video file" << std::endl;
        return 1;
    }
    
    try{
        cv::Mat frame1,frame2;
        std::vector<cv::Rect2d> vec1,vec2;
        video >> frame1;
        video2 >> frame2;
        CamTracking c1(0,trackerType,frame1), c2(1,trackerType,frame2);
        while(video.isOpened() && video2.isOpened()){
            video >> frame1;
            video2 >> frame2;
            vec1 = c1.track(frame1);
            vec2 = c2.track(frame2);

            /*add rectangles around objects*/
            for(int i = 0; i < vec1.size(); i++)
                cv::rectangle(frame1, vec1[i], cv::Scalar(0,255,255), 2, 1);
            for(int i = 0; i < vec2.size(); i++)
                cv::rectangle(frame2, vec2[i], cv::Scalar(0,255,255), 2, 1);

            cv::imshow("tracking 1",frame1);
            cv::imshow("tracking 2",frame2);

            if(cv::waitKey(10)=='c') break;
        }
        
    }catch(const cv::Exception &e){
        std::cout << "***Opencv Error: " << e.what() << std::endl;
    }catch(const std::exception &e){
        std::cout << "***Std Error: "<< e.what() << std::endl;
    }catch(...){
        std::cout << "***Unknown error" << std::endl;
    }
}
