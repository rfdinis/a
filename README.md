# Installation

$ sh install_opencv_contrib.sh

# In alg_comparison or multi_obj:

$ sh compile_cmake.sh

$ ./executable [\<id\>]

# id
0: BOOSTING 

1: MIL

2: KCF

3: TLD

4: MEDIANFLOW

5: GOTURN

6: MOSSE

7: CSRT

# Video files

https://drive.google.com/drive/folders/1Nhxbj20z2W-S8gixcvjVSS2A9-_aK9zt?usp=sharing

(place inside git folder)
