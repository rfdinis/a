TEMPLATE = app
CONFIG += console c++11
CONFIG -= app_bundle
CONFIG -= qt

LIBS += -lopencv_tracking -lopencv_imgproc -lopencv_core -lopencv_video -lopencv_videoio -lopencv_highgui

SOURCES += \
        main.cpp
